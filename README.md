BlendoBox
=========

Thanks for checking out the BlendoBox for Minecraft 1.7.10. You'll be building stuff in no time. But firstly, here's a little rundown (with a changelog up the end).

Are you one of the developers of the mods in this pack? Firstly, thanks for having such amazing stuff! Secondly, if you feel that your mod shouldn't be here, contact me. We'll sort stuff out. :)

Anways, before you download the pack, make sure that you've downloaded and installed Forge version 1208. While other Forge versions may work, this version is geared towards that one. You can grab it from this link:

http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.0.1208/forge-1.7.10-10.13.0.1208-installer-win.exe

Secondly, create a new profile and select the version of Minecraft as Forge 1208. Also, set it to a new directory. Then run it, and it should grab the necessary Minecraft files.

Finally, drag the BlendoBox installer EXE (from the releases part of this pack) into that Minecraft directory, and run it to extract the files.

Now, BlendoBox is currently in a pretty alpha state, so if anything doesn't work, be sure to submit this stuff to the BlendoBox GitHub page under this paragraph. The page also includes a developing wiki, where you can find out all the cool stuff you can do in the modpack. Finally, you can subscribe to the pack to find out when each release comes up.

This readme will also be including a changelog, so you can see this stuff offline too.

Anyways, thanks for playing the BlendoBox. Take care guys!

---

CHANGELOG (in reverse chronological order)

A03 (24/09/14)
- Fix for bug #2 (Greg just needed to update)
- Added Torch Levers and MFFS for cool base defences
- Added IC2 Backpack HUD for a convenient UI item
- Updated a few mods
	- GregTech now doesn't take as long (but still kind-of long) to start up
- Tweaked some ore spawns again to make it a little easier (like re-enabling the vanilla ores (because I've never ever seen GregTech Iron))
- /mods
	- Bibliocraft [1.7.5 -> 1.8.1] updated
	- CodeChickenCore [1.0.3.25 -> 1.0.3.26] updated
	- Forestry [3.0.0.101 -> 3.0.0.128] updated
	- Gregtech [5.04.02 -> 5.05.06] updated
	- IndustrialCraft 2 [2.2.633 -> 2.2.646] updated
	- IndustrialCraft 2 - Advanced Machines removed (due to balancing compatability with GregTech (I'm just worried that it'll either be better than Greg's machines, or too useless))
	- IndustrialCraft 2 Backpack HUD [3.0] added (a nice armour HUD, compatable with all armours in the modpack)
	- MFFS [4.1.0.28] added (testing shielding concepts, might be awesome)
	- NotEnoughItems [1.0.3.51 -> 1.0.3.56] updated
	- Pam's Harvestcraft [1.7.10a -> 1.7.10b] updated
	- Project RED [4.4.8.47 -> 4.4.9.49] updated
	- RailCraft [9.2.2.0 -> 9.3.1.0] updated
	- Reika's Mods [1c -> 1f] updated
	- Resonant Engine [2.0.0.107] added (required for MFFS)
	- Resonant Induction [0.3.4.85] added (for other cool stuff? We'll see)
	- Torch Levers [1.4] added
	- Universal Electricity [4.0.0.53 -> 4.0.0.75] updated
	- Waila [1.5.3a -> 1.5.4a] updated
- /config
	- railcraft/railcraft.cfg: Disabled the new lead spawn, Greg handles that
	- Reika/Electricraft.cfg: Disabled aluminum spawning, Greg handles that
	- IC2BackpackHUD.cfg: Added, changed the position to 5 (you can change this yourself later)
	- MFFS.cfg: Added, no changes for now
	- torchlevers.cfg: Added, no changes for now
	
	
A02 (07/09/14)
- Workaround for bug #1
- Updated Reika's mods from 1b to 1c
- Tweaked config for rough ore spawns
- /mods
	- OpenComputers [1.3.3 -> 1.3.4] updated
	- Reika's Mods [1b -> 1c] updated
		- DragonAPI still crashes, but now only BuildCraft's API has been removed
- /config
	- cofh/ThermalExpansion.cfg: Disables the Thaumcraft API
	- cofh/world/ThermalExpansion-Ores.json: Changed all the ore clusters to 0 (Greg will handle this)
	- forestry/common.cfg: Changed difficulty to NORMAL and made loot rarer. Also, disabled apatite, copper and tin spawns (handled by GregTech)
	- railcraft/railcraft.cfg: Disabled version checking
	- Reika/ElectriCraft.cfg: Disabled ore generation (except for aluminium)
	- Reika/ReactorCraft.cfg: Disabled silver generation
	- AdvancedMachines.cfg: Changed draw factor from 3.0 to 7.0, and the speed up time from 10000 to 20000
	- Bibliocraft.cfg: Allowed empty books to be on bookcases and disabled update checking
	- CodeChickenCore.cfg: Disabled update checking
	- IC2BackpackHUD.cfg: Deleted until mod is updated
	- immibis.cfg: Deleted for now
	- Metallurgy.cfg: Deleted for now
	- MetallurgyCore.cfg: Deleted for now
	- MFFS.cfg: Deleted for now
	- microblocks.cfg: Deleted for now
	- Minestrappolation.cfg: Deleted for now
	- Resonant Engine.cfg: Deleted for now
	- Waila.cfg: Hid entity health for the overlay (workaround for bug #1)
	- weeeflowers.cfg: Changed rarity from 10 to 6

A01 (04/09/14)
- First release of the BlendoBox for v1.7.10
- Nothing else really, just the majority of mods are available
- Most mods are straight from their respective websites, they're not tweaked properly
- /mods
	- Applied Energistics [rv0 build 10] added
	- Applied Energistics - Extra Cells [2.1.3] added
	- Bibliocraft [1.7.5] added
	- Buildcraft [6.0.18] added
	- ChickenCore [1.0.3.25] added
	- COFH Core [3.0.0B6-32] added
	- Extra Binnie [2.0 dev4] added
	- Forge [10.13.0.1208] added
	- Forestry [3.0.0.101] added
	- GregTech [5.04.02] added
	- IndustrialCraft 2 [2.2.617] added
	- IndustrialCraft 2 - Advanced Machines [1.7.10] added
	- IndustrialCraft 2 - Advanced Solar Panels [3.5.1] added
	- Inventory Tweaks [1.59.152] added
	- JABBA [1.1.4] added
	- JourneyMap [4.0.4] added
	- MineFactory Reloaded [2.8.0RC3-591] added
	- NEI Addons [1.12.2.9] added
	- Nether Ores [2.3.0RC3-84] added
	- Not Enough Items [1.0.3.51] added
	- OpenComputers [1.3.3] added
	- OpenEye [0.6] added
	- Pam's Clay Spawn [1.7.2a] added
	- Pam's DesertCraft [1.7.10a] added
	- Pam's HarvestCraft [1.7.10a] added
	- Pam's Temperate Plants [1.7.2b] added
	- Pam's Weee Flowers [1.7.2b] added
	- Project Red [4.4.8.47] added
	- RailCraft [9.2.2.0] added
	- Reika's Mods [1b] added
		- DragonAPI has been modified to delete the BuildCraft and IC2 APIs. This will be fixed soon.
	- Thermal Expansion [4.0.0B5-13] added
	- Thermal Foundation [1.0.0B3-8] added
	- Universal Electricity [4.0.0.53] added
	- Waila [1.5.3a] added
/config
	- OpenEye.json: added "BlendoBox" as a tag (for debugging)
	- Everything else is default for now
